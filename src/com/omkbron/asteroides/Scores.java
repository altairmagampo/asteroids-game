package com.omkbron.asteroides;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class Scores extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scores);
//		setListAdapter(new ArrayAdapter<String>(this, 
//				R.layout.element_lista,
//				R.id.titulo,
//				Asteroides.almacen.listaPuntuaciones(10)));
		setListAdapter(new MiAdaptador(this, Asteroides.almacen.listaPuntuaciones(10)));
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		super.onListItemClick(listView, view, position, id);
		Object o = getListAdapter().getItem(position);
		Toast.makeText(this, "Seleccion: " + Integer.toString(position) 
				+ " - " + o.toString(), Toast.LENGTH_SHORT).show();
	}
	
}
