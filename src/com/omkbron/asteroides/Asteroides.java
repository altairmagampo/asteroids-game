package com.omkbron.asteroides;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Asteroides extends Activity implements OnClickListener {

	private Button bPlay;
	private Button bAbout;
	private Button bScores;
	private Button bPreferences;
	public static AlmacenPuntuaciones almacen = new AlmacenPuntuacionesArray();
	MediaPlayer mp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		bPlay = (Button) findViewById(R.id.button1);
		bPreferences = (Button) findViewById(R.id.button2);
		bAbout = (Button) findViewById(R.id.button3);
		bScores = (Button) findViewById(R.id.button4);

		bAbout.setOnClickListener(this);
		bScores.setOnClickListener(this);
		bPreferences.setOnClickListener(this);
		bPlay.setOnClickListener(this);
		
//		mp = MediaPlayer.create(this, R.raw.audio);

//		Toast.makeText(this, "estas en onCreate", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onSaveInstanceState(Bundle state) {
		super.onSaveInstanceState(state);
		if (mp != null) {
			state.putInt("mpPosition", mp.getCurrentPosition());
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
		if (state != null && mp != null) {
			mp.seekTo(state.getInt("mpPosition"));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mi_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.about:
			launchAbout(null);
			break;
		case R.id.config:
			launchPreferences(null);
			break;
		}
		return true;
	}

	public void launchAbout(View view) {
		launchActivity(view, About.class);
	}

	public void launchPreferences(View view) {
		launchActivity(view, Preferences.class);
	}
	
	public void launchScores(View view) {
		launchActivity(view, Scores.class);
	}
	
	public void launchGame(View view) {
		if (mp.isPlaying()) mp.stop();
		launchActivity(view, Game.class);
	}
	
	public void launchActivity(View view, Class<?> cls) {
		Intent in = new Intent(this, cls);
		startActivity(in);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.button1:
				launchGame(null);
				break;
			case R.id.button2:
				launchPreferences(null);
				break;
			case R.id.button3:
				launchAbout(null);
				break;
			case R.id.button4:
				launchScores(null);
				break;
			default:
				break;
		}
	}
	
	@Override 
	protected void onStart() {
		super.onStart();
		mp = MediaPlayer.create(this, R.raw.audio);
//		Toast.makeText(this, "estas en onStart", Toast.LENGTH_SHORT).show();
	}
		 
	@Override
	protected void onResume() {
		super.onResume();
		if (!mp.isPlaying()) {
			mp.setLooping(true);
			mp.start();
		}
//		Toast.makeText(this, "estas en onResume", Toast.LENGTH_SHORT).show();
	}
		 
	@Override
	protected void onPause() {
//		Toast.makeText(this, "estas en onPause", Toast.LENGTH_SHORT).show();
//		mp.pause();
		super.onPause();
	}

	@Override
	protected void onStop() {
//		Toast.makeText(this, "estas en onStop", Toast.LENGTH_SHORT).show();
		mp.release();
		super.onStop();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
//		Toast.makeText(this, "estas en onRestart", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onDestroy() {
//		Toast.makeText(this, "estas en onDestroy", Toast.LENGTH_SHORT).show();
		super.onDestroy();
	}

}
