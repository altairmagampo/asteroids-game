package com.omkbron.asteroides;

import android.app.Activity;
import android.os.Bundle;

public class Game extends Activity {
	
	private GameView gameView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		
		gameView = (GameView) findViewById(R.id.GameView);	
	}
	
	@Override
	protected void onPause() {
		gameView.pausar();
		gameView.pausarSensores();
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		gameView.reanudar();
		gameView.reanudarSensores();
	}

	@Override
	protected void onDestroy() {
		gameView.detener();
		super.onDestroy();
	}
	
}
