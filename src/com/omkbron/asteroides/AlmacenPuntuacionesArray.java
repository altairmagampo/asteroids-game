package com.omkbron.asteroides;
import java.util.Vector;



public class AlmacenPuntuacionesArray implements AlmacenPuntuaciones {

	private Vector<String> puntuaciones;
	
	public AlmacenPuntuacionesArray() {
		puntuaciones = new Vector<String>();
		puntuaciones.add("1000000 Omar Velasco");
		puntuaciones.add("500000 Velociraptor de Harvard");
		puntuaciones.add("19 Niño de la Selva");
	}
	
	@Override
	public void guardarPuntuacion(int puntos, String nombre, long fecha) {
		puntuaciones.add(0, puntos + " " + nombre);
	}

	@Override
	public Vector<String> listaPuntuaciones(int cantidad) {
		return puntuaciones;
	}

}
